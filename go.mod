module gitlab.com/w1ref1re/ledcore

go 1.14

require (
	github.com/gramework/gramework v1.7.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	go.uber.org/zap v1.15.0
)
