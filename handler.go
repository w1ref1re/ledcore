package ledcore

import (
	"bytes"
	"encoding/json"

	"github.com/gramework/gramework"
	"gitlab.com/w1ref1re/ledcore/apiV1"
	"gitlab.com/w1ref1re/ledcore/internal/config"
	"gitlab.com/w1ref1re/ledcore/models"
	serial "gitlab.com/w1ref1re/ledcore/serial/rpi"
	"go.uber.org/zap"
)

// AddCommandHandler adds a new function and the related endpoint
// it takes a name for the endpoint and the led function
func AddCommandHandler(endpoint string, f func(map[string]interface{}) interface{}) {
	handlerFunc := func(ctx *gramework.Context) interface{} {
		params := make(map[string]interface{})

		err := json.NewDecoder(bytes.NewReader(ctx.PostBody())).Decode(&params)
		if err != nil {
			log.Error("Unable to decode request body", zap.Error(err))
			ctx.BadRequest()
			return map[string]string{
				"error": err.Error(),
			}
		}

		return f(params)
	}

	commandHandlers = append(commandHandlers, models.HandlerFunc{
		Base:     conf.CommandBase,
		Endpoint: endpoint,
		Func:     handlerFunc,
	})
}

func StartServer(c *models.Config) {
	// set config globally
	config.Conf = c

	// connect to arduino
	err := serial.Connect()
	if err != nil {
		log.Fatal("Unable to connect to arduino", zap.Error(err))
	}

	app := gramework.New()

	// === routes === //
	// instruction routes
	app.POST(conf.ApiBase+"/fill", apiV1.FillHandler)
	app.POST(conf.ApiBase+"/clear", apiV1.ClearHandler)
	app.POST(conf.ApiBase+"/set", apiV1.SetHandler)

	// function routes
	registerCommandHandlers(app)
	// === === //

	// TODO:
	// make this configurable

	// pi
	// app.ListenAndServe("192.168.178.43:8080")

	// pc
	app.ListenAndServe("192.168.178.39:8080")
}

func registerCommandHandlers(app *gramework.App) {
	for _, h := range commandHandlers {
		h.Register(app)
	}
}
