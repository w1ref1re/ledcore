// stripe is the interface between the handler logic and the arduino
package stripe

import (
	"gitlab.com/w1ref1re/ledcore/models"
	serial "gitlab.com/w1ref1re/ledcore/serial/rpi"
)

func Clear() (int, error) {
	return serial.SendCommand('c')
}

func Show() (int, error) {
	return serial.SendCommand('s')
}

// Fill fills the stripe
// set @count to 0 to fill the whole stripe
func Fill(color *models.Color, start, count uint32) (int, error) {
	return serial.SendCommand('f', color.AsUint32(), start, count)
}

func SetPixelColor(n uint32, color *models.Color) (int, error) {
	return serial.SendCommand('p', n, color.AsUint32())
}
