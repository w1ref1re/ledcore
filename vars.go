package ledcore

import (
	ilog "gitlab.com/w1ref1re/ledcore/internal/log"
	"gitlab.com/w1ref1re/ledcore/models"
)

var (
	log             = ilog.Log
	commandHandlers = []models.HandlerFunc{}
	conf            *models.Config
)

func NewDefaultConfig(address, port string) *models.Config {
	return &models.Config{
		ServerAddress: address,
		ServerPort:    port,
		ApiBase:       "/api/v1",
		CommandBase:   "/command",

		SerialName:        "/dev/ttyACM0",
		SerialBaud:        9600,
		SerialReadTimeout: 5,
	}
}
