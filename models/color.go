package models

import (
	"encoding/binary"
	"errors"
	"fmt"

	"go.uber.org/zap"
)

var (
	ErrInvalidColor = errors.New("invalid color")

	nilColor = Color{}
)

type Color struct {
	Red, Green, Blue, White uint8
}

func NewColor(red, green, blue, white int) (*Color, error) {
	return &Color{uint8(red), uint8(green), uint8(blue), uint8(white)}, nil

}

func NilColor() *Color {
	return &nilColor
}

func (c *Color) AsUint32() uint32 {
	// since the neopixel uses 'some' color format
	U32 := binary.LittleEndian.Uint32([]uint8{c.Green, c.Blue, c.Red, c.White})
	log.Info("Converting color to UINT32", zap.String("Color", fmt.Sprintf("%+v", (*c))),
		zap.String("UINT", fmt.Sprintf("%x", U32)))
	return U32
}
