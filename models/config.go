package models

type Config struct {
	// Server address
	ServerAddress string
	// Server port
	ServerPort string

	// Base Api path
	ApiBase string
	// Base command endpoint path
	// [ApiBase]/[CommandBase]/[CommandName]
	// Example:
	// /api/v1/cmd/rainbow
	CommandBase string

	/// serial config ///
	// Name of the serial file
	SerialName string
	// Baud
	SerialBaud int
	// ReadTimeout in seconds
	SerialReadTimeout int
}
