package models

import "github.com/gramework/gramework"

type HandlerFunc struct {
	Base, Endpoint string
	Func           func(*gramework.Context) interface{}
}

func (h *HandlerFunc) Register(app *gramework.App) {
	app.POST(h.Base+h.Endpoint, h.Func)
}
