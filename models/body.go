package models

type Body interface {
	IsValid() bool
}

type FillBody struct {
	Red, Green, Blue, White, Start, Count int
}

func (b *FillBody) IsValid() bool {
	return b.Start >= 0 && b.Count >= 0
}

type SetBody struct {
	Red, Green, Blue, White, N int
}

func (b *SetBody) IsValid() bool {
	return true
}
