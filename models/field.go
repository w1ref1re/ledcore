package models

import (
	"errors"
	"strconv"
)

var (
	ErrMissingKey = errors.New("missing key")
)

type Field struct {
	Key, value   string
	defaultValue string // only used when required is false
	Required     bool
}

func NewField(key, defaultValue string, required bool) Field {
	return Field{
		Key:          key,
		defaultValue: defaultValue,
		Required:     required,
	}
}

func (f *Field) ParseField(params map[string][]string) error {
	v, ok := params[f.Key]
	if !ok {
		if !f.Required {
			f.value = f.defaultValue
			return nil
		}

		return ErrMissingKey
	}

	f.value = v[0]
	return nil
}

func (f *Field) GetInt() (int, error) {
	return strconv.Atoi(f.value)
}

func (f *Field) GetFloat() (float64, error) {
	return strconv.ParseFloat(f.value, 64)
}

func (f *Field) GetString() (string, error) {
	return f.value, nil
}

func (f *Field) GetBool() (bool, error) {
	return strconv.ParseBool(f.value)
}
