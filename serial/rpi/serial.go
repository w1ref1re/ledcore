package serial

import (
	// "bytes"
	"encoding/binary"
	"time"

	"github.com/tarm/serial"
	"go.uber.org/zap"
)

func Connect() error {
	var err error

	s, err = serial.OpenPort(&serial.Config{
		Name:        conf.SerialName,
		Baud:        conf.SerialBaud,
		ReadTimeout: time.Duration(conf.SerialReadTimeout) * time.Second,
	})
	if err != nil {
		return err
	}

	time.Sleep(2 * time.Second)

	log.Info("Connected to arduino")

	return nil
}

func Close() {
	s.Close()
}

func SendCommand(cmd byte, values ...uint32) (int, error) {
	// init buffer
	buf := make([]byte, 4)

	for len(values) < 3 {
		values = append(values, 0)
	}

	// add command char
	result := []byte{cmd}

	// append values
	for _, v := range values {
		binary.LittleEndian.PutUint32(buf, v)
		log.Info("Encoding value", zap.Uint32("Value", v), zap.ByteString("Encoded", buf))
		result = append(result, buf...)
	}

	log.Info("Sending command", zap.ByteString("Command", result))

	return Send(result)
}

func Send(bytes []byte) (int, error) {
	return s.Write(bytes)
}
