package serial

import (
	"gitlab.com/w1ref1re/ledcore/internal/config"
	ilog "gitlab.com/w1ref1re/ledcore/internal/log"
	"gitlab.com/w1ref1re/ledcore/models"

	"github.com/tarm/serial"
)

var (
	log  = ilog.Log
	s    *serial.Port
	conf *models.Config
)

func init() {
	conf = config.Conf
}
