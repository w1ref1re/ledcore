package apiV1

import (
	"bytes"
	"encoding/json"

	"github.com/gramework/gramework"
	ilog "gitlab.com/w1ref1re/ledcore/internal/log"
	"gitlab.com/w1ref1re/ledcore/models"
	"go.uber.org/zap"
)

var (
	log = ilog.Log
)

func badRequest(ctx *gramework.Context) map[string]string {
	ctx.BadRequest()
	return map[string]string{
		"status": "invalid request",
	}
}

func internalError(ctx *gramework.Context) map[string]string {
	ctx.Err500()
	return map[string]string{
		"status": "internal server error",
	}
}

func decodeBody(dest models.Body, ctx *gramework.Context) error {
	err := json.NewDecoder(bytes.NewReader(ctx.PostBody())).Decode(dest)
	if err != nil {
		log.Error("Unable to decode request body", zap.Error(err))
		return err
	}

	return nil
}
