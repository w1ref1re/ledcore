package apiV1

import (
	"github.com/gramework/gramework"
	"gitlab.com/w1ref1re/ledcore/models"
	"gitlab.com/w1ref1re/ledcore/stripe"
	"go.uber.org/zap"
)

func FillHandler(ctx *gramework.Context) interface{} {
	body := models.FillBody{}

	// decode request body
	if decodeBody(&body, ctx) != nil {
		return badRequest(ctx)
	}

	color, err := models.NewColor(body.Red, body.Green, body.Blue, body.White)
	if err != nil {
		log.Error("Unable to make color struct", zap.Error(err))
		return badRequest(ctx)
	}

	n, err := stripe.Fill(color, uint32(body.Start), uint32(body.Count))
	if err != nil {
		log.Error("Unable to send Command to arduino", zap.Error(err))
		return internalError(ctx)
	}

	return map[string]interface{}{
		"action":        "fill",
		"bytes_written": n,
		"color":         color,
	}
}

func ClearHandler(ctx *gramework.Context) interface{} {
	n, err := stripe.Clear()
	if err != nil {
		log.Error("Unable to send Command to arduino", zap.Error(err))
		return internalError(ctx)
	}

	return map[string]interface{}{
		"action":        "clear",
		"bytes_written": n,
	}
}

func SetHandler(ctx *gramework.Context) interface{} {
	body := models.SetBody{}

	// decode request body
	if decodeBody(&body, ctx) != nil {
		return badRequest(ctx)
	}

	color, err := models.NewColor(body.Red, body.Green, body.Blue, body.White)
	if err != nil {
		log.Error("Unable to make color struct", zap.Error(err))
		return badRequest(ctx)
	}

	n, err := stripe.SetPixelColor(uint32(body.N), color)
	if err != nil {
		log.Error("Unable to send Command to arduino", zap.Error(err))
		return internalError(ctx)
	}

	return map[string]interface{}{
		"action":        "fill",
		"bytes_written": n,
		"color":         color,
	}
}
